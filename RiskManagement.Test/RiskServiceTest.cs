﻿using NUnit.Framework;
using Ploeh.AutoFixture.NUnit2;
using RiskManagement.DataModel;
using RiskManagement.Test.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ploeh.AutoFixture;
using RiskManagement._Bootstrapper;
using RiskManagement.Interfaces.Repositories;
using Moq;
using Autofac;
using RiskManagement.Interfaces.Services;

namespace RiskManagement.Test
{
    [TestFixture]
    public class RiskServiceTest
    {
        private Mock<IRepository<ResultDataModel>> _mockResultRepository;
        private Mock<IRepository<RiskDataModel>> _mockRiskRepository;

        [SetUp]
        public virtual void SetUp()
        {
            var updater = new ContainerBuilder();

            _mockResultRepository = new Mock<IRepository<ResultDataModel>>();                
            _mockRiskRepository = new Mock<IRepository<RiskDataModel>>();

            updater.RegisterInstance(_mockResultRepository.Object).Keyed<IRepository<ResultDataModel>>("Settled");
            updater.RegisterInstance(_mockRiskRepository.Object).Keyed<IRepository<RiskDataModel>>("Unsettled");

            updater.Update(RMDependencyResolver.Current.Container);
        }

        [Test]
        public void Shouldbeable_To_Generate_Data()
        {
            var data = DataBuilder.GenerateData();

            Assert.IsNotNull(data.Item1);
            Assert.IsNotNull(data.Item2);
        }

        [Test]
        public void Shouldbeable_To_Calculate_Risk()
        {
            var data = DataBuilder.GenerateData();

            _mockResultRepository.Setup(x => x.GetAll()).Returns(data.Item2.AsQueryable());
            _mockRiskRepository.Setup(x => x.GetAll()).Returns(data.Item1.AsQueryable());

            using (var scope = RMDependencyResolver.Current.Container.BeginLifetimeScope())
            {
                var service = scope.Resolve<IRiskService>();

                Assert.IsNotNull(service);

                service.CalculatingRisk();
            }
        }

        [Test]
        public void Shouldbeable_To_Find_Unusal_Customer()
        {
            var data = DataBuilder.GenerateData();

            _mockResultRepository.Setup(x => x.GetAll()).Returns(data.Item2.AsQueryable());
            _mockRiskRepository.Setup(x => x.GetAll()).Returns(data.Item1.AsQueryable());

            using (var scope = RMDependencyResolver.Current.Container.BeginLifetimeScope())
            {
                var service = scope.Resolve<IResultService>();

                Assert.IsNotNull(service);

                service.FindUnusualCustomer();
            }
        }
    }
}
