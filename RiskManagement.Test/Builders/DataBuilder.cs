﻿using Ploeh.AutoFixture;
using RiskManagement.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tynamix.ObjectFiller;

namespace RiskManagement.Test.Builders
{
    public static class DataBuilder
    {
        public static readonly IFixture Fixture;
        private static readonly SequenceGeneratorInt32 SeqA = new SequenceGeneratorInt32 { From = 1, Step = 1 };
        private static readonly SequenceGeneratorInt32 SeqB = new SequenceGeneratorInt32 { From = 1, Step = 1 };

        static DataBuilder()
        {
            Fixture = new Fixture().Customize(new MultipleCustomization());
            Fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            Fixture.Behaviors.Add(new OmitOnRecursionBehavior());                  
        }

        public static Tuple<List<RiskDataModel> , List<ResultDataModel>> GenerateData(int count = 10)
        {
            var risks = new List<RiskDataModel>();
            var results = new List<ResultDataModel>();
            while(count-- > 0)
            {
                var result = DataBuilder.Fixture.Build<ResultDataModel>().Without(x => x.Customer)
                .Create();
                result.Customer = DataBuilder.Fixture.Build<CustomerDataModel>()
                    .With(x => x.Id, result.CustomerId)
                    .Create();

                var risk = DataBuilder.Fixture.Build<RiskDataModel>()
                    .With(x => x.Customer, result.Customer)
                    .Create();

                risks.Add(risk);
                results.Add(result);
            }

            return new Tuple<List<RiskDataModel>, List<ResultDataModel>>(risks, results);
        }
    }
}
