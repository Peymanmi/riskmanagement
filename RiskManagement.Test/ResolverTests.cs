﻿using NUnit.Framework;
using RiskManagement._Bootstrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using RiskManagement.Interfaces.Services;

namespace RiskManagement.Test
{
    [TestFixture]
    public class ResolverTests
    {
        [Test]
        public void Shouldbeable_To_Resolve_ResultService()
        {
            using (var scope = RMDependencyResolver.Current.Container.BeginLifetimeScope())
            {
                var service = scope.Resolve<IResultService>();

                Assert.IsNotNull(service);
            }
        }

        [Test]
        public void Shouldbeable_To_Resolve_RiskService()
        {
            using (var scope = RMDependencyResolver.Current.Container.BeginLifetimeScope())
            {
                var service = scope.Resolve<IRiskService>();

                Assert.IsNotNull(service);
            }
        }
    }
}
