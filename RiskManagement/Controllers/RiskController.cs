﻿using Autofac.Extras.Attributed;
using AutoMapper;
using RiskManagement.DataModel;
using RiskManagement.Interfaces.Controllers;
using RiskManagement.Interfaces.Repositories;
using RiskManagement.Models;
using RiskManagement.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RiskManagement.Extensions;
using RiskManagement.Interfaces.Services;

namespace RiskManagement.Controllers
{
    public class RiskController : Controller, IRiskController
    {
        private readonly IRiskService _riskService;

        public RiskController(IRiskService riskService)
        {
            _riskService = riskService;
        }

        public ActionResult Index()
        {
            var data = _riskService.CalculatingRisk();
            var model = Mapper.Map<List<RiskViewModel>>(data);

            return View(model);
        }
    }
}