﻿using Autofac.Extras.Attributed;
using AutoMapper;
using RiskManagement.DataModel;
using RiskManagement.Interfaces.Controllers;
using RiskManagement.Interfaces.Repositories;
using RiskManagement.Models;
using RiskManagement.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RiskManagement.Extensions;
using RiskManagement.Interfaces.Services;

namespace RiskManagement.Controllers
{
    /// <summary>
    /// Implementing ICustomerController for mocking in test
    /// </summary>
    public class CustomerController : Controller, ICustomerController
    {
        private readonly IResultService _resultService;

        public CustomerController(IResultService resultService)
        {
            _resultService = resultService;
        }

        public ActionResult Index()
        {
            var data = _resultService.FindUnusualCustomer();
            var model = Mapper.Map<List<CustomerViewModel>>(data);

            return View(model);
        }

    }
}
