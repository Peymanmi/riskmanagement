﻿using Autofac.Extras.Attributed;
using RiskManagement.DataModel;
using RiskManagement.Interfaces.Repositories;
using RiskManagement.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RiskManagement.Extensions;

namespace RiskManagement.Services
{
    public class RiskServic : BaseService<RiskDataModel>, IRiskService
    {
        private readonly IResultService _resultService;

        public RiskServic([WithKey("Unsettled")] IRepository<RiskDataModel> betRepository,
            IResultService resultService)
            : base(betRepository)
        {
            _resultService = resultService;
        }

        public IList<RiskDataModel> CalculatingRisk()
        {
            return base.GetAll().AnalyzingRisk(_resultService.GetAll()).ToList();
        }
    }
}