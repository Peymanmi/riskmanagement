﻿using Autofac.Extras.Attributed;
using RiskManagement.DataModel;
using RiskManagement.Interfaces.Repositories;
using RiskManagement.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RiskManagement.Extensions;

namespace RiskManagement.Services
{
    public class ResultService : BaseService<ResultDataModel>, IResultService
    {
        public ResultService([WithKey("Settled")] IRepository<ResultDataModel> resultRepository)
            : base(resultRepository)
        {

        }

        public override IList<ResultDataModel> GetAll()
        {
            return base.GetAll().Analyzing().ToList();
        }

        public IList<ResultDataModel> FindUnusualRate()
        {
            return base.GetAll().Analyzing().Where(x => x.Customer.WonUnsualRate).ToList();
        }

        public IList<CustomerDataModel> FindUnusualCustomer()
        {
            return FindUnusualRate().Select(x => x.Customer).Distinct().ToList();
        }
    }
}