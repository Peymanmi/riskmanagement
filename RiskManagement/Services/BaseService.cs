﻿using RiskManagement.DataModel;
using RiskManagement.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using RiskManagement.Extensions;

namespace RiskManagement.Services
{
    public class BaseService<TData>
        where TData : BaseDataModel
    {
        private readonly IRepository<TData> _repository;

        public BaseService(IRepository<TData> repository)
        {
            _repository = repository;
        }

        public virtual IList<TData> GetAll()
        {
            return _repository.GetAll().ToList();
        }

        public virtual IList<TData> Find(Expression<Func<TData, bool>> expression)
        {
            return _repository.Find(expression).ToList();
        }
    }
}