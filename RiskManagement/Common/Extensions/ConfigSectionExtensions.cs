﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace RiskManagement.Extensions
{
    public static class ConfigSectionExtensions
    {
        public static IEnumerable<T> AsEnumerable<T>(this ConfigurationElementCollection coll)
        {
            foreach (var o in coll)
                if (o != null)
                    yield return (T)o;
        }
    }
}