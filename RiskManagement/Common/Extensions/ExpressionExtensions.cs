﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using RiskManagement.DataModel;

namespace RiskManagement.Extensions
{
    public static class ExpressionExtensions
    {
        public static IEnumerable<RiskDataModel> AnalyzingRisk(this IEnumerable<RiskDataModel> query, IEnumerable<ResultDataModel> result)
        {
            query.ToList().ForEach(data =>
                {
                    data.Customer = result.Select(x => x.Customer).FirstOrDefault(x => x.Id == data.CustomerId);
                });

            return query;
        }

        public static IEnumerable<ResultDataModel> Analyzing(this IEnumerable<ResultDataModel> query)
        {
            var customers = new List<CustomerDataModel>();
            query.ToList().ForEach(data =>
            {
                if (!customers.Any(x => x.Id == data.CustomerId))
                {
                    data.Customer.NumbetOfWin = query.Count(x => x.CustomerId == data.CustomerId && x.IsWin);
                    data.Customer.NumbetOfBetting = query.Count(x => x.CustomerId == data.CustomerId);
                    data.Customer.TotalStake = query.Where(x => x.CustomerId == data.CustomerId).Sum(x => x.Stake);
                    data.Customer.TotalWin = query.Where(x => x.CustomerId == data.CustomerId).Sum(x => x.Amount);
                    data.Customer.AverageStake = (float)data.Customer.TotalStake / data.Customer.NumbetOfBetting;
                    customers.Add(data.Customer);
                }

                data.Customer = customers.Single(x => x.Id == data.CustomerId);
            });

            return query;
        }
    }
}