﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiskManagement.DataModel;
using System.Linq.Expressions;

namespace RiskManagement.Interfaces.Repositories
{
    public interface IRepository<TData> 
        where TData : BaseDataModel
    {
        IQueryable<TData> GetAll();

        IQueryable<TData> Find(Expression<Func<TData, bool>> expression);
    }
}
