﻿using RiskManagement.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiskManagement.Interfaces.Services
{
    public interface IRiskService : IBaseService<RiskDataModel>
    {
        IList<RiskDataModel> CalculatingRisk();
    }
}