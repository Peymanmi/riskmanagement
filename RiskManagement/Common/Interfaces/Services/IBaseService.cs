﻿using RiskManagement.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace RiskManagement.Interfaces.Services
{
    public interface IBaseService<TData>
        where TData : BaseDataModel
    {
        IList<TData> GetAll();

        IList<TData> Find(Expression<Func<TData, bool>> expression);
    }
}