﻿using RiskManagement.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiskManagement.Interfaces.Services
{
    public interface IResultService : IBaseService<ResultDataModel>
    {
        IList<ResultDataModel> FindUnusualRate();

        IList<CustomerDataModel> FindUnusualCustomer();
    }
}