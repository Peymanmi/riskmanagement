﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiskManagement.DataModel
{
    public class ResultDataModel : BetDataModel
    {
        public bool IsWin
        {
            get
            {
                return Amount > Stake;
            }
        }        
    }
}