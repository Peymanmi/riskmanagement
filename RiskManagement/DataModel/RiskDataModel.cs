﻿using RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiskManagement.DataModel
{
    public class RiskDataModel : BetDataModel
    {        
        public bool CanWonLargeAmount { get { return Amount >= Constants.LARGEAMOUNTCANWIN; } }

        public bool IsRisky { get { return Customer.WonUnsualRate; } }

        public bool IsUnusual { get { return Amount >= Customer.AverageStake * 10; } }

        public bool IsHighlyUnusual { get { return Amount >= Customer.AverageStake * 30; } }
    }
}