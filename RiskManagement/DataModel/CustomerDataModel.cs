﻿using RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiskManagement.DataModel
{
    public class CustomerDataModel : BaseDataModel
    {
        public int NumbetOfBetting { get; set; }

        public int NumbetOfWin { get; set; }

        public int TotalWin { get; set; }

        public int TotalStake { get; set; }

        public float AverageStake { get; set; }

        public bool WonUnsualRate { get { return (decimal)NumbetOfWin / NumbetOfBetting >= Constants.UNUSALWONRATE; } }
    }
}