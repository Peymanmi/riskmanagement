﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiskManagement.DataModel
{
    public class BetDataModel : BaseDataModel
    {
        public int CustomerId { get; set; }

        public int EventId { get; set; }

        public int ParticipantId { get; set; }

        public int Stake { get; set; }

        public int Amount { get; set; }

        public CustomerDataModel Customer { get; set; }
    }
}