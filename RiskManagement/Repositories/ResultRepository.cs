﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RiskManagement.DataModel;
using LinqToExcel;
using System.Configuration;
using RiskManagement.Extensions;
using System.Linq.Expressions;
using RiskManagement.Interfaces.Repositories;

namespace RiskManagement.Repositories
{
    public class ResultRepository : BaseRepository<ResultDataModel>, IRepository<ResultDataModel>
    {

        public ResultRepository()
            : base(ConfigurationManager.AppSettings["SettledExcel"], "Settled")
        {
            Initialize();
        }

        protected override void Configure()
        {            
            Excel.AddMapping<ResultDataModel>(x => x.EventId, "Event");
            Excel.AddMapping<ResultDataModel>(x => x.CustomerId, "Customer");
            Excel.AddMapping<ResultDataModel>(x => x.Amount, "Win");
            Excel.AddMapping<ResultDataModel>(x => x.ParticipantId, "Participant");
            Excel.AddMapping<ResultDataModel>(x => x.Customer, "Customer", (s) =>
            {
                var cust = new CustomerDataModel() { Id = int.Parse(s) };
                return cust;
            });
        }
    }
}