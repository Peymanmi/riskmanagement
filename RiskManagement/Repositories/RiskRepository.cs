﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RiskManagement.DataModel;
using LinqToExcel;
using System.Configuration;
using RiskManagement.Interfaces.Repositories;

namespace RiskManagement.Repositories
{
    public class RiskRepository : BaseRepository<RiskDataModel>, IRepository<RiskDataModel>
    {

        public RiskRepository()
            : base(ConfigurationManager.AppSettings["UnsettledExcel"], "Unsettled")
        {
            Initialize();
        }

        protected override void Configure()
        {
            Excel.AddMapping<ResultDataModel>(x => x.EventId, "Event");
            Excel.AddMapping<ResultDataModel>(x => x.CustomerId, "Customer");
            Excel.AddMapping<ResultDataModel>(x => x.Amount, "To Win");
            Excel.AddMapping<ResultDataModel>(x => x.ParticipantId, "Participant");
            Excel.AddMapping<ResultDataModel>(x => x.Customer, "Customer", (s) =>
            {
                var cust = new CustomerDataModel() { Id = int.Parse(s) };
                return cust;
            });
        }
    }
}