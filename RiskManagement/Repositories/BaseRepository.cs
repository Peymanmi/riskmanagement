﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RiskManagement.DataModel;
using RiskManagement.Interfaces.Repositories;
using LinqToExcel;
using System.IO;
using System.Linq.Expressions;

namespace RiskManagement.Repositories
{
    public class BaseRepository<TData> : IRepository<TData>
        where TData : BaseDataModel
    {
        private string _fileName;
        private string _sheetName;
        protected ExcelQueryFactory Excel;

        public BaseRepository(string fileName)
            : this(fileName, null)
        {

        }

        public BaseRepository(string fileName, string sheetName)
        {
            if (File.Exists(fileName))
                this._fileName = fileName;
            else
            {
                this._fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
            }
            this._sheetName = sheetName;
        }

        protected void Initialize()
        {
            Excel = new ExcelQueryFactory(_fileName);
            Configure();
        }

        protected virtual void Configure()
        {
        }

        protected string SheetName
        {
            get
            {                
                if (string.IsNullOrEmpty(_sheetName))
                    return Excel.GetWorksheetNames().First();
                else
                    return _sheetName;
            }
        }

        public virtual IQueryable<TData> GetAll()
        {
            return Excel.Worksheet<TData>(SheetName);
        }


        public virtual IQueryable<TData> Find(Expression<Func<TData, bool>> expression)
        {
            return Excel.Worksheet<TData>(SheetName)
                .Where(expression);
        }
    }
}