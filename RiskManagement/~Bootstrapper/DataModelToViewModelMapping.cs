﻿using AutoMapper;
using RiskManagement.DataModel;
using RiskManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiskManagement._Bootstrapper
{
    public class DataModelToViewModelMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<RiskDataModel, RiskViewModel>()
                ;

            Mapper.CreateMap<CustomerDataModel, CustomerViewModel>()
                ;
        }
    }
}