﻿using Autofac;
using Bootstrap;
using Bootstrap.AutoMapper;
using Bootstrap.Autofac;
using Bootstrap.Extensions.StartupTasks;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using RiskManagement.Extensions;

namespace RiskManagement._Bootstrapper
{
    public class RMDependencyResolver
    {
        private static readonly IContainer _container;
        private static readonly RMDependencyResolver _current;
        private static readonly object _locker = new object();

        static RMDependencyResolver()
        {
            lock (_locker)
            {
                if (_current == null || _container == null)
                {
                    _current = new RMDependencyResolver();
                    _container = RMDependencyResolver.Initialize();
                }
            }
        }

        public static RMDependencyResolver Current { get { return _current; } }

        public IContainer Container
        {
            get { return _container; }
        }

        private static IContainer Initialize()
        {
            var assemblies = new List<Assembly>();
            var configSection = ConfigurationManager.GetSection("bootstrapperModules") as BootstrapperRegistrationConfigurationSection;
            if (configSection != null && configSection.Modules != null && configSection.Modules.Count > 0)
                configSection.Modules.AsEnumerable<BootstrapperConfigurationElement>().ForEach(e => assemblies.Add(Assembly.Load(e.Assembly)));

            Bootstrapper
                .With.AutoMapper()
                .And.Autofac()
                .And.StartupTasks()
                .IncludingOnly.AssemblyRange(assemblies)
                .Start();

            return (IContainer)Bootstrapper.Container;

        }

    }
}