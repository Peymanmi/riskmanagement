﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace RiskManagement._Bootstrapper
{
    public class BootstrapperRegistrationConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("modules")]
        public BootstrapperConfigurationElementCollection Modules { get { return ((BootstrapperConfigurationElementCollection)base["modules"]); } }
    }

    [ConfigurationCollection(typeof(BootstrapperConfigurationElement))]
    public class BootstrapperConfigurationElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement() { return new BootstrapperConfigurationElement(); }
        protected override object GetElementKey(ConfigurationElement element) { return element; }
        public BootstrapperConfigurationElement this[int idx] { get { return (BootstrapperConfigurationElement)BaseGet(idx); } }
    }

    public class BootstrapperConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name { get { return (string)base["name"]; } set { base["name"] = value; } }
        [ConfigurationProperty("assembly", IsKey = true, IsRequired = true)]
        public string Assembly { get { return (string)base["assembly"]; } set { base["assembly"] = value; } }
    }
}