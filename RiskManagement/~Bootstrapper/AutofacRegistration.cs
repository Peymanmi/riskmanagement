﻿using Autofac;
using Autofac.Extras.Attributed;
using Autofac.Integration.Mvc;
using RiskManagement.Controllers;
using RiskManagement.DataModel;
using RiskManagement.Interfaces.Controllers;
using RiskManagement.Interfaces.Repositories;
using RiskManagement.Interfaces.Services;
using RiskManagement.Repositories;
using RiskManagement.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RiskManagement._Bootstrapper
{
    public class AutofacRegistration : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<RiskRepository>().Keyed<IRepository<RiskDataModel>>("Unsettled");
            builder.RegisterType<ResultRepository>().Keyed<IRepository<ResultDataModel>>("Settled");
            
            builder.RegisterType<RiskServic>().As<IRiskService>().WithAttributeFilter();
            builder.RegisterType<ResultService>().As<IResultService>().WithAttributeFilter();


            builder.RegisterType<CustomerController>().InstancePerRequest();
            builder.RegisterType<RiskController>().InstancePerRequest();
        }
    }
}