﻿using Autofac;
using Autofac.Integration.Mvc;
using Bootstrap.Extensions.StartupTasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RiskManagement._Bootstrapper
{
    [Task(PositionInSequence = 1, Group = 1)]
    public class WarmUpTasks : Module, IStartupTask
    {
        private ContainerBuilder _builder;

        protected override void Load(ContainerBuilder builder)
        {
            _builder = builder;

            _builder.RegisterType<WarmUpTasks>().As<IStartupTask>().InstancePerLifetimeScope();
        }
        public void Run()
        {
            var container = Bootstrap.Bootstrapper.Container as IContainer;
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        public void Reset()
        {
            //throw new NotImplementedException();
        }
    }
}