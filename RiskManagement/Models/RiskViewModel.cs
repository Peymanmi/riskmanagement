﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiskManagement.Models
{
    public class RiskViewModel
    {
        public int CustomerId { get; set; }

        public int EventId { get; set; }

        public int ParticipantId { get; set; }

        public int Stake { get; set; }

        public int Amount { get; set; }


        public bool CanWonLargeAmount { get; set; }

        public bool IsRisky { get; set; }

        public bool IsUnusual { get; set; }

        public bool IsHighlyUnusual { get; set; }
    }
}