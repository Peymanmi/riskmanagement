﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiskManagement.Models
{
    public class CustomerViewModel
    {
        public int Id { get; set; }

        public int NumbetOfBetting { get; set; }

        public int NumbetOfWin { get; set; }

        public int TotalWin { get; set; }

        public int TotalStake { get; set; }
    }
}