# README #

## What is this repository for? ##

* This repository has been created for TechChallenge
* 1.0.0

### How do I get set up? ###


* Make sure Nuget Command already installed, if not you can install it through this link https://docs.nuget.org/consume/command-line-reference
* In root directory try to restore all packages


```
#!c#
   nuget.exe restore
```

### Configuration ###

  There are two conflagration keys for  storing CSV file's path as follow:
  
```xml
  <add key="SettledExcel" value="DataFiles\Settled.csv" />
  <add key="UnsettledExcel" value="DataFiles\Unsettled.csv" />
```

### Dependencies ###

  All dependencies are manage by Nuget, you can find the list of them as below:

  1. BootStrapper https://www.nuget.org/packages/Bootstrapper/

  2. Autofac https://www.nuget.org/packages/Autofac/

  3. AutoMapper https://www.nuget.org/packages/AutoMapper/

  4. LinqToExcel https://www.nuget.org/packages/LinqToExcel/

**Also Test project is using following package:**

  1. Tynamix.ObjectFiller https://www.nuget.org/packages/Tynamix.ObjectFiller/

  2. AutoFixtire https://www.nuget.org/packages/AutoFixture

  3. Moq https://www.nuget.org/packages/Moq/
  
  4. NUnit https://www.nuget.org/packages/NUnit/
  

### All test are based on NUnit, So you can  run them through NUnit Test runner 

1. NUnit Gui Runner (http://www.nunit.org/index.php?p=nunit-gui&r=2.4)

2. NUnit Test Adapter (https://visualstudiogallery.msdn.microsoft.com/6ab922d0-21c0-4f06-ab5f-4ecd1fe7175d)